package com.mobiwolf.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.mobiwolf.contactselector.ContactSelectorAsyncActivity;
import com.mobiwolf.contactselector.R;

public class TestActivity extends Activity {

	private static final int CHOOSE_CONTACT = 0;
	private static final boolean POPULATE_CONTACTS = false;
	private final OnClickListener listener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(TestActivity.this,
					ContactSelectorAsyncActivity.class);
			startActivityForResult(intent, CHOOSE_CONTACT);

		}
	};

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK && data.getExtras() != null) {
			String mail = data
					.getStringExtra(ContactSelectorAsyncActivity.E_MAIL);
			String number = data
					.getStringExtra(ContactSelectorAsyncActivity.PHONE_NUMBER);

			if (mail != null) {

				Toast.makeText(this, "Email " + mail, Toast.LENGTH_SHORT)
						.show();
			}
			if (number != null) {
				Toast.makeText(this, "Phone number " + number,
						Toast.LENGTH_SHORT).show();
			}
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sample_activity_test);
		findViewById(R.id.root).setOnClickListener(listener);
		if (POPULATE_CONTACTS) {
			ContactPopulator contactPopulator = new ContactPopulator(this);
			contactPopulator.create(900, 1, 1);

			int count = contactPopulator.countContacts();
			Toast.makeText(this, "Contacts: " + count, Toast.LENGTH_LONG)
					.show();
		}
	}
}
