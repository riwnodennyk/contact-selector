package com.mobiwolf.sample;

import java.security.SecureRandom;
import java.util.ArrayList;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

public class ContactPopulator {
	private final ContentResolver mContentResolver;
	private final SecureRandom mRandom = new SecureRandom();
	
	private ArrayList <ContentProviderOperation> mOperations;
	
	private final static char[] CHARS_ALPHA = "abcdefghjiklmnopqrstuvwxyz".toCharArray();
	private final static char[] CHARS_DIGITS = "0123456789".toCharArray();
	
	private final int[] TYPE_PHONE = {ContactsContract.CommonDataKinds.Phone.TYPE_HOME, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE, ContactsContract.CommonDataKinds.Phone.TYPE_WORK};
	private final int[] TYPE_EMAIL = {ContactsContract.CommonDataKinds.Email.TYPE_HOME, ContactsContract.CommonDataKinds.Email.TYPE_MOBILE, ContactsContract.CommonDataKinds.Email.TYPE_WORK};
	
	public ContactPopulator(Context context){		
		mContentResolver = context.getContentResolver();
	}
	
	public void create(int amountContacts, int amountEmails, int amountPhones){
		String displayName;
		for(int i=0;i<amountContacts;i++){
			displayName = generateRandomString(8, CHARS_ALPHA)+" "+generateRandomString(9, CHARS_ALPHA);
			createContact(displayName, amountEmails, amountPhones);
		}
	}
	
	public int countContacts(){
		Cursor cursor = mContentResolver.query(
				ContactsContract.Contacts.CONTENT_URI,
				null,
				null,
				null,
				null);
		int count = cursor.getCount();
		cursor.close();
		return count;
	}
	
	public void createContact(String displayName, int amountEmails, int amountPhones){
		mOperations = new ArrayList<ContentProviderOperation>();
		
		mOperations.add(ContentProviderOperation.newInsert(
				 ContactsContract.RawContacts.CONTENT_URI)
				     .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
				     .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
				     .build());		
		
		mOperations.add(ContentProviderOperation.newInsert(
			     ContactsContract.Data.CONTENT_URI)
			         .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
			         .withValue(ContactsContract.Data.MIMETYPE,
			     ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
			         .withValue(
			     ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME,
			     displayName).build());
		
		String email;
		for(int i=0;i<amountEmails;i++){
			email = generateRandomString(7, CHARS_ALPHA)+"@"+generateRandomString(5, CHARS_ALPHA)+".com";
			addEmail(email, TYPE_EMAIL[i%TYPE_EMAIL.length]);
		}
		
		String phone;
		for (int i = 0; i < amountPhones; i++) {
			phone = generateRandomString(10, CHARS_DIGITS);
			addPhone(phone, TYPE_PHONE[i%TYPE_PHONE.length]);
		}
		
		try {
			mContentResolver.applyBatch(ContactsContract.AUTHORITY, mOperations);
		 } catch (Exception e) {
		     e.printStackTrace();
		 } 
	}
	
	private void addEmail(String email, int type){
		mOperations
				.add(ContentProviderOperation
						.newInsert(ContactsContract.Data.CONTENT_URI)
						.withValueBackReference(
								ContactsContract.Data.RAW_CONTACT_ID, 0)
						.withValue(
								ContactsContract.Data.MIMETYPE,
								ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE)
						.withValue(ContactsContract.CommonDataKinds.Email.DATA,
								email)
						.withValue(ContactsContract.CommonDataKinds.Email.TYPE,
								type).build());
	}
	
	private void addPhone(String phone, int type){
		mOperations.add(ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
		         .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
		         .withValue(ContactsContract.Data.MIMETYPE,
		     ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
		         .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone)
		         .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, type)
		         .build());
	}
	
	private String generateRandomString(int size, char[] characters){
		StringBuffer sb = new StringBuffer();
		int pos;
		for (int i=0;i<size;i++){
			pos = mRandom.nextInt(characters.length);
			sb.append(characters[pos]);
		}
		return sb.toString();
	}
}
