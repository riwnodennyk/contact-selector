package com.mobiwolf.contactselector;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.mobiwolf.loading.AsyncString.DataItem;
import com.mobiwolf.loading.PhoneMailCache.ImageCacheParams;
import com.mobiwolf.loading.PhoneMailWorker;
import com.mobiwolf.loading.PhoneMailWorker.DataMode;

public class ContactSelectorAsyncActivity extends FragmentListActivity
		implements LoaderCallbacks<Cursor> {
	class DialogDataLoader extends AsyncTask<Long, Void, List<DataItem>> {

		private final Context mContext;
		private final String mName;

		public DialogDataLoader(Context context, String name) {
			this.mContext = context;
			this.mName = name;
		}

		@Override
		protected List<DataItem> doInBackground(Long... idArray) {
			long id = idArray[0];
			List<DataItem> stringArray = new ArrayList<DataItem>();
			Cursor sourcePhones = getContentResolver().query(
					ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
					new String[] {

					ContactsContract.CommonDataKinds.Phone.NUMBER },
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = "
							+ id, null, null);
			while (sourcePhones.moveToNext()) {
				stringArray.add(new DataItem(DataMode.ONE_PHONE, sourcePhones
						.getString(0)));
			}
			sourcePhones.close();
			Cursor sourceMails = getContentResolver()
					.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
							new String[] { ContactsContract.CommonDataKinds.Email.ADDRESS },

							ContactsContract.CommonDataKinds.Email.CONTACT_ID
									+ " = " + id, null, null);
			while (sourceMails.moveToNext()) {
				stringArray.add(new DataItem(DataMode.ONE_MAIL, sourceMails
						.getString(0)));
			}
			sourceMails.close();
			return stringArray;
		}

		@Override
		protected void onPostExecute(List<DataItem> result) {
			super.onPostExecute(result);
			Log.d(LOG_TAG, "onPostExecute beg");
			AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
			builder.setTitle(mName);

			ListView modeList = new ListView(mContext);
			modeList.setBackgroundColor(Color.WHITE);

			final ArrayAdapter<DataItem> modeAdapter = new ArrayAdapter<DataItem>(
					mContext, android.R.layout.simple_list_item_1,
					android.R.id.text1, result);

			modeList.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					DataItem item = (modeAdapter.getItem(position));
					switch (item.mKey) {
					case DataMode.ONE_MAIL:
						finishActivityWith(E_MAIL, item.mValue);
						break;
					case DataMode.ONE_PHONE:
						finishActivityWith(PHONE_NUMBER, item.mValue);
						break;
					default:
						throw new IllegalArgumentException("Unknown key.");

					}
				}
			});
			modeList.setAdapter(modeAdapter);

			builder.setView(modeList);
			mDialog = builder.create();
			mDialog.show();
		}
	}

	class ListAdapter extends CursorAdapter {

		private Integer mIdIndex;
		private final LayoutInflater mInflater;
		private Integer mNameIndex;

		public ListAdapter(Context context, Cursor c, boolean autoRequery) {
			super(context, c, autoRequery);
			mInflater = LayoutInflater.from(context);
		}

		public ListAdapter(Context context, Cursor c, int flags) {
			super(context, c, flags);
			mInflater = LayoutInflater.from(context);
		}

		@Override
		public void bindView(View view, Context context, Cursor cursor) {
			((TextView) view.findViewById(R.id.name)).setText(cursor
					.getString(mNameIndex));
			Long id = cursor.getLong(mIdIndex);
			mImageFetcher.loadContactValues(id,
					(TextView) view.findViewById(R.id.value),
					(TextView) view.findViewById(R.id.mode));
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			if (mNameIndex == null) {
				mIdIndex = cursor.getColumnIndex(ID);
				mNameIndex = cursor.getColumnIndex(DISPLAY_NAME);
			}
			return mInflater.inflate(R.layout.part_contact_selector, parent,
					false);
		}
	}

	public static final String E_MAIL = "email";
	private static final int LOADER_ID = 721;

	private static final String LOG_TAG = ContactSelectorAsyncActivity.class
			.getSimpleName();

	public static final String PHONE_NUMBER = "phone number";

	private static String generateWhereClause(List<String> whereClauseArray) {
		if (whereClauseArray.size() == 0) {
			return null;
		} else {
			StringBuilder stringBuilder = new StringBuilder();
			for (String whereClause : whereClauseArray) {
				stringBuilder.append(" AND ");
				stringBuilder.append(whereClause);
			}
			stringBuilder.delete(0, 5);
			return stringBuilder.toString();
		}
	}

	String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
	String ID = ContactsContract.Contacts._ID;

	private View mNothingFound;
	private View mProgress;
	private TextView mSearchView;

	private AlertDialog mDialog;

	private PhoneMailWorker mImageFetcher;

	private String mSearchText;

	private void finishActivityWith(String key, String value) {
		Intent intent = new Intent();
		intent.putExtra(key, value);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	protected int getSupportLayoutResourceId() {
		return R.layout.activity_contact_selector;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_contact_selector);

		mSearchView = ((TextView) findViewById(R.id.search));
		mSearchView.addTextChangedListener(new TextWatcher() {

			@Override
			public void afterTextChanged(Editable editable) {
				mSearchText = editable.toString();
				getSupportLoaderManager().restartLoader(LOADER_ID, null,
						ContactSelectorAsyncActivity.this);
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}
		});

		mNothingFound = findViewById(R.id.nothing_found);
		mProgress = findViewById(R.id.progress);

		ImageCacheParams cacheParams = new ImageCacheParams();

		cacheParams.setMemCacheSizePercent(0.25f);

		mImageFetcher = new PhoneMailWorker(getContentResolver());
		mImageFetcher.addImageCache(getSupportFragmentManager(), cacheParams);
		getSupportLoaderManager().initLoader(LOADER_ID, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int arg0, Bundle arg1) {
		// setListAdapter(null);
		setLoading();

		boolean hasConstaint = mSearchText != null;

		List<String> whereClauseArray = new ArrayList<String>();

		if (hasConstaint) {
			whereClauseArray.add(ContactsContract.Contacts.DISPLAY_NAME
					+ " LIKE '%" + mSearchText + "%'");
		}

		return new CursorLoader(this, ContactsContract.Contacts.CONTENT_URI,
				new String[] { ContactsContract.Contacts._ID,
						ContactsContract.Contacts.DISPLAY_NAME },
				generateWhereClause(whereClauseArray), null,
				ContactsContract.Contacts.DISPLAY_NAME + " ASC");
	}

	@Override
	protected void onListItemClick(ListView l, View view, int position, long id) {
		super.onListItemClick(l, view, position, id);
		String name = ((TextView) view.findViewById(R.id.name)).getText()
				.toString();
		int mode = Integer.parseInt(((TextView) view.findViewById(R.id.mode))
				.getText().toString());
		String value = ((TextView) view.findViewById(R.id.value)).getText()
				.toString();

		switch (mode) {
		case DataMode.NONE:
			break;
		case DataMode.ONE_MAIL:
			finishActivityWith(E_MAIL, value);
			break;
		case DataMode.ONE_PHONE:
			finishActivityWith(PHONE_NUMBER, value);
			break;
		case DataMode.MULTIPLE:
			new DialogDataLoader(this, name).execute(id);
			break;
		default:
			throw new IllegalArgumentException("Mode not recognized.");
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor cursor) {
		if (cursor.getCount() < 1) {
			setNothingFound();
		}
		CursorAdapter adapter = new ListAdapter(
				ContactSelectorAsyncActivity.this, cursor,
				android.R.layout.simple_list_item_1);
		setListAdapter(adapter);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mDialog != null)
			mDialog.dismiss();
	}

	void setLoading() {
		mNothingFound.setVisibility(View.GONE);
		mProgress.setVisibility(View.VISIBLE);
	}

	void setNothingFound() {
		mNothingFound.setVisibility(View.VISIBLE);
		mProgress.setVisibility(View.GONE);
	}

}
