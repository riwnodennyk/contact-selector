package com.mobiwolf.loading;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import com.mobiwolf.loading.PhoneMailWorker.BitmapWorkerTask;
import com.mobiwolf.loading.PhoneMailWorker.DataMode;

public class AsyncString {
	private static final String NONE = "None";
	private static final String CHOOSE_ONE = "Choose one";

	public static class DataItem {
		public int mKey;
		public String mValue;

		public DataItem(int key, String value) {
			mKey = key;
			mValue = value;
		}

		@Override
		public String toString() {
			return mValue;
		}
	}

	private WeakReference<BitmapWorkerTask> mBitmapWorkerTaskReference = null;
	private List<DataItem> stringArray = null;

	public AsyncString() {
	}

	public AsyncString(BitmapWorkerTask bitmapWorkerTask) {
		mBitmapWorkerTaskReference = new WeakReference<BitmapWorkerTask>(
				bitmapWorkerTask);
	}

	public void add(DataItem dataItem) {
		if (stringArray == null) {
			stringArray = new ArrayList<DataItem>();
		}
		stringArray.add(dataItem);
	}

	public BitmapWorkerTask getBitmapWorkerTask() {
		return mBitmapWorkerTaskReference.get();
	}

	final public int getMode() {
		if (stringArray == null || stringArray.size() == 0) {
			return DataMode.NONE;
		} else if (stringArray.size() == 1) {
			return stringArray.get(0).mKey;
		} else {
			return DataMode.MULTIPLE;
		}
	}

	@Override
	public String toString() {
		if (stringArray == null || stringArray.size() == 0) {
			return NONE;
		} else if (stringArray.size() == 1) {
			return stringArray.get(0).mValue;
		} else {
			return CHOOSE_ONE;
		}
	}
}