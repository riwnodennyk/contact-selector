/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobiwolf.loading;

import java.lang.ref.WeakReference;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.util.Pair;
import android.widget.ImageView;
import android.widget.TextView;

import com.mobiwolf.contactselector.BuildConfig;
import com.mobiwolf.loading.AsyncString.DataItem;

public class PhoneMailWorker {

	public class BitmapWorkerTask extends AsyncTask<Long, Void, AsyncString> {
		private Long key;
		private final WeakReference<TextView> modeTextViewReference;
		private final WeakReference<TextView> valueTextViewReference;

		public BitmapWorkerTask(TextView valueTextView, TextView modeTextView) {
			valueTextViewReference = new WeakReference<TextView>(valueTextView);
			modeTextViewReference = new WeakReference<TextView>(modeTextView);
		}

		/**
		 * Background processing.
		 */
		@Override
		protected AsyncString doInBackground(Long... params) {
			if (BuildConfig.DEBUG) {
				Log.d(TAG, "doInBackground - starting work");
			}

			key = params[0];
			AsyncString bitmap = null;

			// Wait here if work is paused and the task is not cancelled
			synchronized (mPauseWorkLock) {
				while (mPauseWork && !isCancelled()) {
					try {
						mPauseWorkLock.wait();
					} catch (InterruptedException e) {
					}
				}
			}

			if (bitmap == null && !isCancelled()
					&& getAttachedTextView() != null && !mExitTasksEarly) {
				bitmap = processBitmap(key);
			}

			if (bitmap != null && mContactsPhoneMailCache != null) {
				mContactsPhoneMailCache.addBitmapToCache(key, bitmap);
			}

			if (BuildConfig.DEBUG) {
				Log.d(TAG, "doInBackground - finished work");
			}

			return bitmap;
		}

		private Pair<TextView, TextView> getAttachedTextView() {
			final TextView imageView = valueTextViewReference.get();
			final TextView modeImageView = modeTextViewReference.get();
			final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

			if (this == bitmapWorkerTask) {
				return new Pair<TextView, TextView>(imageView, modeImageView);
			}

			return null;
		}

		@Override
		protected void onCancelled(AsyncString bitmap) {
			super.onCancelled(bitmap);
			synchronized (mPauseWorkLock) {
				mPauseWorkLock.notifyAll();
			}
		}

		@Override
		protected void onPostExecute(AsyncString value) {
			// if cancel was called on this task or the "exit early" flag is set
			// then we're done
			if (isCancelled() || mExitTasksEarly) {
				value = null;
			}

			final Pair<TextView, TextView> textView = getAttachedTextView();
			if (value != null && textView != null) {
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "onPostExecute - setting bitmap");
				}
				setValues(textView.first, textView.second, value);
			}
		}

	}

	public class CacheAsyncTask extends AsyncTask<Object, Void, Void> {

		@Override
		protected Void doInBackground(Object... params) {
			switch ((Integer) params[0]) {
			case MESSAGE_CLEAR:
				clearCacheInternal();
				break;
			}
			return null;
		}
	}

	public interface DataMode {
		public static final int MULTIPLE = 3;
		public static final int NONE = 0;
		public static final int ONE_MAIL = 1;
		public static final int ONE_PHONE = 2;
	}

	private static final String LOG_TAG = PhoneMailWorker.class.getSimpleName();
	private static final int MESSAGE_CLEAR = 0;
	private static final String TAG = "ImageWorker";

	private final static String LOADING = "Loading...";

	/**
	 * Returns true if the current work has been canceled or if there was no
	 * work in progress on this image view. Returns false if the work in
	 * progress deals with the same data. The work is not stopped in that case.
	 */
	public static boolean cancelPotentialWork(Object data, TextView imageView) {
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

		if (bitmapWorkerTask != null) {
			final Object bitmapData = bitmapWorkerTask.key;
			if (bitmapData == null || !bitmapData.equals(data)) {
				bitmapWorkerTask.cancel(true);
				if (BuildConfig.DEBUG) {
					Log.d(TAG, "cancelPotentialWork - cancelled work for "
							+ data);
				}
			} else {
				// The same work is already in progress.
				return false;
			}
		}
		return true;
	}

	/**
	 * Cancels any pending work attached to the provided ImageView.
	 * 
	 * @param imageView
	 */
	public static void cancelWork(TextView imageView) {
		final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);
		if (bitmapWorkerTask != null) {
			bitmapWorkerTask.cancel(true);
			if (BuildConfig.DEBUG) {
				final Object bitmapData = bitmapWorkerTask.key;
				Log.d(TAG, "cancelWork - cancelled work for " + bitmapData);
			}
		}
	}

	/**
	 * @param imageView
	 *            Any imageView
	 * @return Retrieve the currently active work task (if any) associated with
	 *         this imageView. null if there is no such task.
	 */
	private static BitmapWorkerTask getBitmapWorkerTask(TextView imageView) {
		if (imageView != null) {
			if (imageView.getTag() instanceof AsyncString) {
				final AsyncString asyncDrawable = (AsyncString) imageView
						.getTag();
				return asyncDrawable.getBitmapWorkerTask();
			}
		}
		return null;
	}

	private PhoneMailCache mContactsPhoneMailCache;

	private final ContentResolver mContentResolver;

	private boolean mExitTasksEarly = false;

	private PhoneMailCache.ImageCacheParams mImageCacheParams;

	protected boolean mPauseWork = false;

	private final Object mPauseWorkLock = new Object();

	public PhoneMailWorker(ContentResolver contentResolver) {
		mContentResolver = contentResolver;
	}

	/**
	 * Adds an {@link PhoneMailCache} to this worker in the background (to prevent
	 * disk access on UI thread).
	 * 
	 * @param fragmentManager
	 * @param cacheParams
	 */
	public void addImageCache(FragmentManager fragmentManager,
			PhoneMailCache.ImageCacheParams cacheParams) {
		mImageCacheParams = cacheParams;
		setPhoneMailCache(PhoneMailCache.findOrCreateCache(fragmentManager,
				mImageCacheParams));
	};

	public void clearCache() {
		new CacheAsyncTask().execute(MESSAGE_CLEAR);
	}

	protected void clearCacheInternal() {
		if (mContactsPhoneMailCache != null) {
			mContactsPhoneMailCache.clearCache();
		}
	}

	public void loadContactValues(Long key, TextView valueImageView,
			TextView modeImageView) {
		if (key == null) {
			return;
		}

		AsyncString asyncString = null;

		if (mContactsPhoneMailCache != null) {
			asyncString = mContactsPhoneMailCache.getValueFromMemCache(key);
		}

		if (asyncString != null) {
			// Getting values from the memory cache instead of DB query
			valueImageView.setText(asyncString.toString());
			modeImageView.setText("" + asyncString.getMode());
		} else if (cancelPotentialWork(key, valueImageView)) {
			final BitmapWorkerTask task = new BitmapWorkerTask(valueImageView,
					modeImageView);
			final AsyncString asyncDrawable = new AsyncString(task);
			valueImageView.setText(LOADING);
			valueImageView.setTag(asyncDrawable);

			task.executeOnExecutor(AsyncTask.DUAL_THREAD_EXECUTOR, key);
		}
	}

	/**
	 * Subclasses should override this to define any processing or work that
	 * must happen to produce the final bitmap. This will be executed in a
	 * background thread and be long running. For example, you could resize a
	 * large bitmap here, or pull down an image from the network.
	 * 
	 * @param data
	 *            The data to identify which image to process, as provided by
	 *            {@link PhoneMailWorker#loadContactValues(Object, ImageView)}
	 * @return The processed bitmap
	 */
	protected AsyncString processBitmap(Long id) {
		AsyncString asyncString = new AsyncString();
		Cursor sourcePhones = mContentResolver.query(
				ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
				new String[] {

				ContactsContract.CommonDataKinds.Phone.NUMBER },
				ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id,
				null, null);
		while (sourcePhones.moveToNext()) {
			asyncString.add(new DataItem(DataMode.ONE_PHONE, sourcePhones
					.getString(0)));
		}
		sourcePhones.close();
		Cursor sourceMails = mContentResolver
				.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
						new String[] { ContactsContract.CommonDataKinds.Email.ADDRESS },

						ContactsContract.CommonDataKinds.Email.CONTACT_ID
								+ " = " + id, null, null);
		while (sourceMails.moveToNext()) {
			asyncString.add(new DataItem(DataMode.ONE_MAIL, sourceMails
					.getString(0)));
		}
		sourceMails.close();
		Log.d(LOG_TAG, "doInBackground end");
		return asyncString;
	}

	public void setExitTasksEarly(boolean exitTasksEarly) {
		mExitTasksEarly = exitTasksEarly;
	}

	public void setPhoneMailCache(PhoneMailCache imageCache) {
		mContactsPhoneMailCache = imageCache;
	}

	public void setPauseWork(boolean pauseWork) {
		synchronized (mPauseWorkLock) {
			mPauseWork = pauseWork;
			if (!mPauseWork) {
				mPauseWorkLock.notifyAll();
			}
		}
	}

	/**
	 * Called when the processing is complete and the final bitmap should be set
	 * on the ImageView.
	 * 
	 * @param valueTextView
	 * @param value
	 */
	private void setValues(TextView valueTextView, TextView modeTextView,
			AsyncString value) {
		valueTextView.setText(value.toString());
		modeTextView.setText("" + value.getMode());
	}
}
